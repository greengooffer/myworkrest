﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Grow.Services.Goals
{
    [ServiceContract]
    public interface IGoalsService
    {
        [OperationContract]
        [WebGet(UriTemplate = "data?s={s}")]
        string GetData(string s);

        [OperationContract]
        [WebInvoke(UriTemplate = "data", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        string PostData(string s);
    }
}
