﻿using Grow.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Grow.Services.Goals
{
    [GlobalErrorBehaviour(typeof(GlobalErrorHandler))]
    public class GoalsService : IGoalsService
    {
        public string GetData(string s)
        {
            return $"Hello from Grow.Services.Goals!  {s}";
        }

        public string PostData(string s)
        {
            return string.Format("Hello from Grow.Services.Goals!");
        }
    }
}
