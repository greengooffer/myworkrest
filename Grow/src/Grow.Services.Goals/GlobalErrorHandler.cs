﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Services.Goals
{
    public class GlobalErrorHandler : IErrorHandler
    {
        public void ProvideFault(Exception error, MessageVersion version, ref Message message)
        {
            var fault = new Grow.Services.Identity.Fault
            {
                Message = error.Message
            };
            toJson(fault, ref message);
        }
        void toJson(Grow.Services.Identity.Fault fault, ref Message message)
        {
            message = WebOperationContext.Current.CreateJsonResponse<Fault>((Fault)fault, new DataContractJsonSerializer(typeof(Fault)));
            var response = WebOperationContext.Current.OutgoingResponse;
            response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            response.StatusDescription = "Custom Fault";
        }
        public bool HandleError(Exception error)
        {
            return true;
        }
    }
}