﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;
using System.Runtime.Serialization.Json;
using System.Net;

namespace Grow.Common
{
    public class GlobalErrorHandler : IErrorHandler
    {
        public void ProvideFault(Exception error, MessageVersion version, ref Message message)
        {
            var fault = new Fault
            {
                Message = error.Message
            };
            toJson(fault, ref message);
        }
        void toJson(Fault fault, ref Message message)
        {
            message = WebOperationContext.Current.CreateJsonResponse<Fault>((Fault)fault, new DataContractJsonSerializer(typeof(Fault)));
            var response = WebOperationContext.Current.OutgoingResponse;
            response.StatusCode = HttpStatusCode.InternalServerError;
            response.StatusDescription = "Custom Fault";
        }
        public bool HandleError(Exception error)
        {
            return true;
        }

        [DataContract]
        public class Fault
        {
            public const string Action = "Error";
            [DataMember]
            public string Message
            {
                get;
                set;
            }
        }
    }
}