﻿using System;
using System.Runtime.Serialization;
using System.Security;

namespace Grow.Common 
{
    [Serializable]
    public class GrowException : Exception
    {
        public string Code { get; }
        
        public GrowException(string code, string message)
            : base(message)
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(code));
            }

            Code = code;
        }
        
        public GrowException()
        {
        }

        public GrowException(string message) : base(message)
        {
        }

        public GrowException(string message, Exception innerException) : base(message, innerException)
        {
        }

        [SecuritySafeCritical]
        protected GrowException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
