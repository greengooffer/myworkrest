﻿using Serilog;
using System.Diagnostics;
using System.Web.Http.ExceptionHandling;

namespace Grow.Api.Controllers
{
    public class GlobalExceptionLogger : ExceptionLogger
    {
        public override void Log(ExceptionLoggerContext context)
        {
            Serilog.Log.Error(context.Exception, context.Exception.Message);
        }
    }
}
