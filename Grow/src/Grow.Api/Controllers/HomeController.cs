﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Grow.Api.Controllers
{
    [RoutePrefix("api/home")]
    public class HomeController : ApiController
    {
        public string GetData(string s)
        {
            return $"Hello from Grow.Api!  {s}";
        }
        public string PostData(string s)
        {
            return string.Format("Hello from Grow.Api!");
        }
        [Route("exception")]
        [HttpPost]
        public void PostException()
        {
            throw new Exception("Error happenes!");
        }
    }
}
