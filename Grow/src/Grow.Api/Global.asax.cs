﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using Serilog;

namespace Grow.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .WriteTo.File(@"C:\Intel\GrowLog\GrowWebApi\growwebapi.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();

            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
