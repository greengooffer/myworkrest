﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace Grow.Services.Identity
{
    [TestClass()]
    public class UserRepositoryTests
    {
        [TestInitialize]
        public void RepoInitialize()
        {
            using (var context = new IdentityDbContext())
            {
                var initUser = new User { Id = Guid.NewGuid(), Email = "billy.m@gmail.com", Name = "Billy", Password = "Passw#" };
                context.Users.Add(initUser);
                context.SaveChanges();
            }
        }

        [TestCleanup]
        public void TrancateTable()
        {
            using (var context = new IdentityDbContext())
            {
                context.Database.ExecuteSqlCommand("TRUNCATE TABLE dbo.Users");
            }
        }

        [TestMethod()]
        public async Task AddAsyncTestAsync()
        {
            UserRepository repo = new UserRepository();
            User checkUser = new User { Id = Guid.NewGuid(), Email = "addy@gmail.com", Name = "Addy", Password = "Passw#" };
            await repo.AddAsync(checkUser);
            User getUser = repo.GetAsync("addy@gmail.com").Result;
            Assert.AreEqual(checkUser.Name, getUser.Name);
        }

        [TestMethod()]
        public void GetAsyncTest()
        {
            UserRepository repo = new UserRepository();
            User checkUser = repo.GetAsync("marco@gmail.com").Result;
            
            Assert.IsNull(checkUser);
        }

        [TestMethod()]
        public void GetAsyncTest1()
        {
            UserRepository repo = new UserRepository();
            User checkUser = repo.GetAsync("34534523452").Result;

            Assert.IsNull(checkUser);
        }
    }
}