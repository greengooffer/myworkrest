﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Grow.Services.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Services.Identity
{
    [TestClass()]
    public class IdentityDbContextTests
    {
        [TestMethod()]
        public void IdentityDbContextTest()
        {
            using (var context = new IdentityDbContext())
            {
                var testUser = new User { Id = Guid.NewGuid(), Email = "test@gmail.com", Name = "Billy", Password = "Passw#" };
                context.Users.Add(testUser);
                context.SaveChanges();

                var user = context.Users.FindAsync(testUser.Id).Result;
                Assert.IsNotNull(user);
                Assert.AreEqual(user.Email, testUser.Email);
            }
        }
    }
}