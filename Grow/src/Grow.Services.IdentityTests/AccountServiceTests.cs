﻿using System;
using System.Threading.Tasks;
using Grow.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Grow.Services.Identity
{
    [TestClass()]
    public class AccountServiceTests
    {
        [TestMethod()]
        [ExpectedException(typeof(GrowException))]
        public async Task RegisterAsyncTest_UserExists()
        {            
            var mock = new Mock<IUserRepository>();
            mock.Setup(a => a.GetAsync(It.IsAny<string>())).ReturnsAsync(() => new User());
            var service = new AccountService(mock.Object);            
            await service.RegisterAsync("moqa@gmail.com", "Moqa", "Passw#");
        }

        [TestMethod()]
        public async Task RegisterAsyncTest_UserRegister()
        {
            // Arrange
            var mock = new Mock<IUserRepository>();         
            mock.Setup(a => a.GetAsync(It.IsAny<string>())).ReturnsAsync(() => null);
            mock.Setup(b => b.AddAsync(It.IsAny<User>())).Returns(() => Task.FromResult(true));
            var service = new AccountService(mock.Object);
            // Act
            await service.RegisterAsync("moqa@gmail.com", "Passw#", "Moqa");
            // Assert
            mock.Verify(foo => foo.AddAsync(It.Is<User>(x => x.Name == "Moqa" && x.Password == "Passw#" && x.Email == "moqa@gmail.com")), Times.Once());
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod()]
        public async Task RegisterAsyncTest_NullValue()
        {
            // Failing on check values step, thats why await is ok.
            var mock = new Mock<IUserRepository>();
            mock.Setup(a => a.GetAsync(It.IsAny<string>())).ReturnsAsync(() => new User());
            var service = new AccountService(mock.Object);
            await service.RegisterAsync(null, "Moqa", "Passw#");
        }

        [TestMethod()]
        [ExpectedException(typeof(NotImplementedException))]
        public void LoginAsyncTest()
        {
            var mock = new Mock<IUserRepository>();
            new AccountService(mock.Object).LoginAsync(string.Empty, string.Empty);
        }
    }
}