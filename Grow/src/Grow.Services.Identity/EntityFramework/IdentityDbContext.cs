namespace Grow.Services.Identity
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class IdentityDbContext : DbContext
    {
        public IdentityDbContext()
            : base("name=IdentityDbContext")
        {
        }

        public virtual DbSet<User> Users { get; set; }
    } 
}