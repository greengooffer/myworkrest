﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Grow.Services.Identity
{
    public class UserRepository : IUserRepository
    {
        public async Task AddAsync(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            using (var context = new IdentityDbContext())
            {
                context.Users.Add(user);
                await context.SaveChangesAsync();
            }
        }

        public async Task DeleteAsync(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                throw new ArgumentException("Email could not be null or empty", nameof(email));
            }

            using (var context = new IdentityDbContext())
            {
                var user = await context.Users.FirstOrDefaultAsync(x => x.Email == email);
                if (user != null)
                {
                    context.Users.Remove(user);
                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task<IEnumerable<User>> GetAsync()
        {
            using (var context = new IdentityDbContext())
            {
                var users = await context.Users.ToListAsync();
                return users;
            }
        }

        public async Task<User> GetAsync(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentException("ID cannot be empty");
            }

            using (var context = new IdentityDbContext())
            {
                var user = await context.Users.FindAsync(id);
                return user;
            }
        }

        public async Task<User> GetAsync(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                throw new ArgumentException("Email cannot be empty", nameof(email));
            }

            using (var context = new IdentityDbContext())
            {
                var user = await context.Users.FirstOrDefaultAsync(x => x.Email == email);
                return user;
            }
        }
    }
}