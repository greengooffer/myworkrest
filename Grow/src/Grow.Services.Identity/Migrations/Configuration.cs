namespace Grow.Services.Identity.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Grow.Services.Identity.IdentityDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Grow.Services.Identity.IdentityDbContext context)
        {

            User user = new User { Id = Guid.NewGuid(), Email = "admin@gmail.com", Name = "Admin", Password = "Passw#" };
            context.Users.Add(user);
        }
    }
}
