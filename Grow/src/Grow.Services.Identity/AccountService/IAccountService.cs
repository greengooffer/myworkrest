﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Services.Identity;

namespace Grow.Services.Identity
{
    public interface IAccountService
    {
        Task RegisterAsync(string email, string password, string name);
        Task<JsonWebToken> LoginAsync(string email, string password); // leave this one unimplemented
    }
}
