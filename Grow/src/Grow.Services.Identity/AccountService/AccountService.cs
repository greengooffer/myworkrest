﻿using System;
using System.Threading.Tasks;
using Grow.Common;
using Grow.Services.Identity;
using GrowException = Grow.Common.GrowException;

namespace Grow.Services.Identity
{
    public class AccountService : IAccountService
    {
        private readonly IUserRepository _repo;

        public AccountService(IUserRepository repo)
        {
            _repo = repo ?? throw new ArgumentException("Value can not be null");
        }

        public async Task RegisterAsync(string email, string password, string name)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                throw new ArgumentException("Email is empty", nameof(email));
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                throw new ArgumentException("Password is empty", nameof(password));
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("Name is Empty", nameof(name));
            }

            User checkUser = await _repo.GetAsync(email);
            if (checkUser != null)
            {
                throw new GrowException("user_exists", "User already exists");
            }
            var user = new User { Id = Guid.NewGuid(), Email = email, Name = name, Password = password };
            await _repo.AddAsync(user);
        }

        public Task<JsonWebToken> LoginAsync(string email, string password)
        {
            throw new NotImplementedException();
        }
    }
}