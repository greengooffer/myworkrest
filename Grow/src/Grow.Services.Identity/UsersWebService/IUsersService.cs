﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using Grow.Common;

namespace Grow.Services.Identity
{
    [ServiceContract]
    public interface IUsersService
    {
        [OperationContract]
        [WebGet(UriTemplate = "users?email={email}")]
        Task<User> GetUser(string email);

        [OperationContract]
        [WebGet(UriTemplate = "users")]
        Task<IEnumerable<User>> GetUsers();

        [OperationContract]
        [WebInvoke(UriTemplate = "users?email={email}&password={password}&name={name}", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        Task AddUser(string email, string password, string name);

        [OperationContract]
        [WebInvoke(UriTemplate = "users?email={email}", Method = "DELETE", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        Task DeleteUser(string email);
    }
}

