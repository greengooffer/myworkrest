﻿using Grow.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Services.Identity
{
    [GlobalErrorBehaviour(typeof(GlobalErrorHandler))]
    public class UsersService : IUsersService
    {
        public async Task<User> GetUser(string email)
        {
            UserRepository repo = new UserRepository();
            User user = await repo.GetAsync(email);
            return user;
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            UserRepository repo = new UserRepository();
            var users = await repo.GetAsync();
            return users;
        }

        public async Task AddUser(string email, string password, string name)
        {
            AccountService repo = new AccountService(new UserRepository());
            await repo.RegisterAsync(email, password, name);
        }

        public async Task DeleteUser(string email)
        {
            UserRepository repo = new UserRepository();
            await repo.DeleteAsync(email);
        }
    }
}
